%define gobuild(o:) %{expand:
  %global _dwz_low_mem_die_limit 0
  %ifnarch ppc64
  go build -buildmode pie -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-}%{?currentgoldflags} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '-Wl,-z,relro  -Wl,-z,now' -compressdwarf=false" -a -v -x %{?**}; 
  %else
  go build -buildmode pie -compiler gc -tags="rpm_crashtraceback libtrust_openssl ${BUILDTAGS:-}" -ldflags "-linkmode=external -compressdwarf=false ${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags'" -a -v -x %{?**};
  %endif
}

%global with_debug 1

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%global gomodulesmode GO111MODULE=on

%global git0 https://github.com/containers/%{name}

Name:           buildah
Version:        1.34.1
Release:        5
Summary:        A command line tool used for creating OCI Images
License:        Apache-2.0 and BSD-2-Clause and BSD-3-Clause and ISC and MIT and MPL-2.0
URL:            https://%{name}.io
Source:         %{git0}/archive/refs/tags/v%{version}.tar.gz
Source1:        https://github.com/cpuguy83/go-md2man/archive/refs/tags/v2.0.2.tar.gz

Patch0001:      0001-fix-CVE-2024-24786.patch
Patch0002:      0002-fix-CVE-2024-1753.patch
Patch0003:      0003-fix-CVE-2024-28180.patch
Patch0004:      0004-fix-CVE-2024-3727.patch

BuildRequires:  device-mapper-devel
BuildRequires:  git-core
BuildRequires:  golang >= 1.16.6
BuildRequires:  glib2-devel
BuildRequires:  glibc-static
#BuildRequires:  go-rpm-macros
BuildRequires:  gpgme-devel
BuildRequires:  libassuan-devel
BuildRequires:  make
BuildRequires:  ostree-devel
BuildRequires:  btrfs-progs-devel
BuildRequires:  shadow-subid-devel
Requires:       containers-common
BuildRequires:  libseccomp-static
Requires:       libseccomp >= 2.4.1-0
Suggests:       cpp

%description
The %{name} package provides a command line tool which can be used to
* create a working container from scratch
or
* create a working container from an image as a starting point
* mount/umount a working container's root file system for manipulation
* save container's root file system layer to create a new image
* delete a working container or an image

%package tests
Summary: Tests for %{name}

Requires: %{name} = %{version}-%{release}
Requires: bats
Requires: bzip2
Requires: podman
Requires: golang
Requires: jq
Requires: httpd-tools
Requires: openssl
Requires: nmap-ncat
Requires: git-daemon

%description tests
%{summary}

This package contains system tests for %{name}

%prep
%autosetup -Sgit -n %{name}-%{version} -p1
tar -xf %SOURCE1

%build
GO_MD2MAN_PATH="$(pwd)%{_bindir}"
mkdir -p _buildgo2man/bin $GO_MD2MAN_PATH
cd go-md2man-*
go build -mod=vendor -o ../_buildgo2man/bin/go-md2man .
cp ../_buildgo2man/bin/go-md2man $GO_MD2MAN_PATH/go-md2man
export PATH=$GO_MD2MAN_PATH:$PATH
cd -

%set_build_flags
export GOFLAGS=-mod=vendor
export GOPATH=$(pwd)/_build:$(pwd)
export CGO_CFLAGS=$CFLAGS
# These extra flags present in $CFLAGS have been skipped for now as they break the build
CGO_CFLAGS=$(echo $CGO_CFLAGS | sed 's/-flto=auto//g')
CGO_CFLAGS=$(echo $CGO_CFLAGS | sed 's/-Wp,D_GLIBCXX_ASSERTIONS//g')
CGO_CFLAGS=$(echo $CGO_CFLAGS | sed 's/-specs=\/usr\/lib\/rpm\/redhat\/redhat-annobin-cc1//g')

%ifarch x86_64
export CGO_CFLAGS+=" -m64 -mtune=generic -fcf-protection=full"
%endif

export CNI_VERSION=`grep '^# github.com/containernetworking/cni ' vendor/modules.txt | sed 's,.* ,,'`
export LDFLAGS="-X main.buildInfo=`date +%s` -X main.cniVersion=${CNI_VERSION}"

export BUILDTAGS='seccomp exclude_graphdriver_devicemapper $(hack/systemd_tag.sh) $(hack/libsubid_tag.sh)'
export BUILDTAGS+=" btrfs_noversion exclude_graphdriver_btrfs"

%gobuild -o bin/%{name} ./cmd/%{name}
%gobuild -o bin/imgtype ./tests/imgtype
%gobuild -o bin/copy ./tests/copy
%gobuild -o bin/tutorial ./tests/tutorial
%{__make} docs

%install
make DESTDIR=%{buildroot} PREFIX=%{_prefix} install install.completions

install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav tests/. %{buildroot}/%{_datadir}/%{name}/test/system
cp bin/imgtype %{buildroot}/%{_bindir}/%{name}-imgtype
cp bin/copy    %{buildroot}/%{_bindir}/%{name}-copy
cp bin/tutorial %{buildroot}/%{_bindir}/%{name}-tutorial

rm %{buildroot}%{_datadir}/%{name}/test/system/tools/build/*

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%files tests
%license LICENSE
%{_bindir}/%{name}-imgtype
%{_bindir}/%{name}-copy
%{_bindir}/%{name}-tutorial
%{_datadir}/%{name}/test

%changelog
* Thu Dec 26 2024 jianmin <jianmin@iscas.ac.cn> - 1.34.1-5
- Type:cve
- CVE:CVE-2024-3727 CVE-2024-24791
- SUG:NA
- DESC: fix CVE-2024-3727 and Rebuild to fix CVE-2024-24791
* Tue Apr 23 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.34.1-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-28180

* Thu Apr 11 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.34.1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-1753

* Wed Apr 10 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.34.1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-24786

* Wed Feb 28 2024 chendexi <chendexi@kylinos.cn> - 1.34.1-1
- Upgrade to 1.34.1

* Mon Jan 15 2024 lijian <lijian2@kylinos.cn> - 1.30.0-1
- Upgrade to 1.30.0

* Mon Jun 20 2022 fushanqing <fushanqing@kylinos.cn> - 1.26.1-1
- Initial package
